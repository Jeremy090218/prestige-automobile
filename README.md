Développement en duo avec Sébastien Zoppello.
Projet de Licence Pro Applications Web. 

Développement d'un site Web fictif d'exposition de voitures aux enchères. 

Création en JavaScript, site OnePage. 
Objectifs de référencements, application des normes Google dans cette optique (par exemple, simulation d'une URL pour les pages internes, afin que le site OnePage paraisse en avoir plusieurs, et pour permettre l'actualisation dans les pages internes). 
Données sous format JSON.

Vous pouvez accéder au site hébergé en ligne depuis le lien suivant :
https://prestigeautomobile.zoppello.fr/